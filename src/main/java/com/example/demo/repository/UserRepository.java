package com.example.demo.repository;

import com.example.demo.model.User;

import java.util.HashMap;
import java.util.Map;

public class UserRepository {

    private final Map<Integer, User> usersDatabase;
    private final int MAX_LOGIN_ATTEMPTS = 3;

    public enum AuthorizationStatus {
        ACCESS,
        UNAUTHORIZED,
        FORBIDDEN
    }

    public UserRepository() {
        usersDatabase = new HashMap<>();

        usersDatabase.put(1, new User("cracker", "cracker1234", true, 0));
        usersDatabase.put(2, new User("marry", "marietta!#09", true, 0));
        usersDatabase.put(3, new User("silver", "$silver$", true, 0));
    }

    public AuthorizationStatus checkLogin(final String login, final String password) {

        for (Integer key : usersDatabase.keySet()) {

            User user = usersDatabase.get(key);

            if (user.getLogin().equals(login)) {

                if (!user.isActive()) {

                    return AuthorizationStatus.FORBIDDEN;
                }

                if (!user.getPassword().equals(password)) {

                    user.setIncorrectLoginCounter(user.getIncorrectLoginCounter() + 1);
                    user.setActive(user.getIncorrectLoginCounter() < MAX_LOGIN_ATTEMPTS);
                    usersDatabase.put(key, user);

                    return AuthorizationStatus.UNAUTHORIZED;
                }

                if (user.getPassword().equals(password) && user.getIncorrectLoginCounter() < 3) {

                    user.setIncorrectLoginCounter(0);       // zerowanie licznika prób po poprawnym zalogowaniu
                    usersDatabase.put(key, user);
                    return AuthorizationStatus.ACCESS;

                }
            }
        }

        return AuthorizationStatus.UNAUTHORIZED;
    }

}
